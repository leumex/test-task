<%--
  Created by IntelliJ IDEA.
  User: Asus
  Date: 08.09.2019
  Time: 19:09
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<body>
<p>
    <c:choose>
    <c:when test="${not empty foundCars}">
<h2>Search results</h2>
<table>
    <tr>
        <th>Car number</th>
        <th>Registration time</th>
    </tr>
    <c:forEach var="car" items="${foundCars}">
        <tr>
            <td><c:out value="${car.carNumber}"/></td>
            <td><c:out value="${car.timestamp}"/></td>
        </tr>
    </c:forEach>
</table>
</c:when>
<c:otherwise>
    <c:out value="No car found!"/>
</c:otherwise>
</c:choose>
</p>

<p>
<form action="/" method="get">
    <button type="submit">Back to menu</button>
</form>
</p>
</body>
</html>
