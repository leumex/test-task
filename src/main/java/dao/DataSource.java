package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DataSource {

    private Connection connection;

    {
        try {
            connection = DriverManager.getConnection("jdbc:h2:mem:db1;DB_CLOSE_DELAY=-1;",
                    "car", "car");
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    static {
        try {
            Class.forName("org.h2.Driver");
        } catch (ClassNotFoundException e) {
            System.out.println(e.getMessage() + " Driver is NOT registered!");
        }
    }

    public static final DataSource ds = new DataSource();

    public DataSource() {
    }

    public Connection getConnection() throws SQLException  {
        if(connection != null) return connection; else throw new SQLException();
    }


}
