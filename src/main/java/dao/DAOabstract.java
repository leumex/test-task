package dao;

import bean.RegisteredCar;

import java.util.List;

public interface DAOabstract {
    List<RegisteredCar> read();
    void insert (RegisteredCar car);
    int count();
}
