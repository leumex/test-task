package dao;

import bean.RegisteredCar;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class DAOImpl implements DAOabstract {

    public static final DAOImpl DAO = new DAOImpl();


    static {
        try {
            DataSource.ds.getConnection().createStatement().execute("drop table if exists registered_cars;" +
                    "create table registered_cars (id int NOT NULL AUTO_INCREMENT, carNumber varchar, timestamp timestamp);");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<RegisteredCar> read() {
        List<RegisteredCar> cars = new ArrayList<>();
        try {
            Connection con = DataSource.ds.getConnection();
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery("select * from registered_cars;");
            while (rs.next()) {
                cars.add(new RegisteredCar(rs.getString(2),
                        rs.getTimestamp(3)));
            }
            rs.close();
            st.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return cars;
    }

    @Override
    public void insert(RegisteredCar car) {
        try {
            Connection con = DataSource.ds.getConnection();
            PreparedStatement st = con.prepareStatement("insert into registered_cars(carNumber,timestamp) values (?,?);");
            st.setString(1, car.getCarNumber());
            st.setTimestamp(2, car.getTimestamp());
            st.executeUpdate();
            st.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public int count() {
        int result = 0;
        try {
            Connection con = DataSource.ds.getConnection();
            Statement st = con.createStatement();
            ResultSet resultSet = st.executeQuery("select count (*) from registered_cars;");
            resultSet.next();
            result = resultSet.getInt("count(*)");
            resultSet.close();
            st.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }
}
