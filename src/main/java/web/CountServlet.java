package web;

import dao.DAOImpl;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "CountServlet",
        description = "Count of registered cars",
        urlPatterns = "/registeredCars/count")
public class CountServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException  {
        req.setAttribute("quantity", DAOImpl.DAO.count());
        RequestDispatcher rd = req.getRequestDispatcher("/WEB-INF/count.jsp");
        rd.forward(req, resp);
    }
}
