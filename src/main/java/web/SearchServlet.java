package web;

import bean.RegisteredCar;
import dao.DAOImpl;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@WebServlet(name = "SearchServlet",
        description = "Select cars",
        urlPatterns = "/registeredCars/")
public class SearchServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String carNumber = req.getParameter("carNumber");
        String[] date = req.getParameter("Date").split("-");
        LocalDate queryDate = LocalDate.of(Integer.valueOf(date[0]), Integer.valueOf(date[1]), Integer.valueOf(date[2]));
        List<RegisteredCar> baseList = DAOImpl.DAO.read();
        List<RegisteredCar> foundList = new ArrayList<>();
        for (RegisteredCar car : baseList) {
            if (car.getCarNumber().contains(carNumber)) {
                if (queryDate.equals(car.getTimestamp().toLocalDateTime().toLocalDate()))
                    foundList.add(car);
            }
        }

        req.setAttribute("foundCars", foundList);
        RequestDispatcher requestDispatcher = req.getRequestDispatcher("/WEB-INF/filter.jsp");
        requestDispatcher.forward(req, resp);
    }
}
