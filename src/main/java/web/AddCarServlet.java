package web;

import bean.RegisteredCar;
import dao.DAOImpl;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Timestamp;

@WebServlet(name = "AddServlet",
        description = "Register next car",
        urlPatterns = "/registeredCars")
public class AddCarServlet extends HttpServlet {

    private static final long serialVersionUID = 66L;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
        RequestDispatcher requestDispatcher = req.getRequestDispatcher("/WEB-INF/add.jsp");
        requestDispatcher.forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
            String carNumber = req.getParameter("carNumber");
            RegisteredCar car = new RegisteredCar(carNumber, new Timestamp(System.currentTimeMillis()));
            DAOImpl.DAO.insert(car);
            req.setAttribute("carNumber", car.getCarNumber());
            req.setAttribute("time", car.getTimestamp());
            doGet(req,resp);
    }
}

