package bean;

import java.sql.Timestamp;

public class RegisteredCar {
    private String carNumber;
    private Timestamp timestamp;


    public String getCarNumber() {
        return carNumber;
    }

    public void setCarNumber(String carNumber) {
        this.carNumber = carNumber;
    }

    public Timestamp getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp;
    }

    public RegisteredCar(String carNumber, Timestamp timestamp) {
        this.carNumber = carNumber;
        this.timestamp = timestamp;
    }
}
